var express = require("express");
var app = express();

// var Connection = require('tedious').Connection;
// var Request = require('tedious').Request;

var router = express.Router();


var config = {
  userName: 'v1User', // update me
  password: 'corkCloud1', // update me
  server: 'awesomeinnovation.database.windows.net', // update me
  options: {
      database: 'iotdb', //update me
      encrypt: true
  }
};

// var connection = new Connection(config);

// Attempt to connect and execute queries if connection goes through
// connection.on('connect', function(err) {
//     if (err) {
//         console.log(err);
//     }
//     else{
//         queryDatabase();
//     }
// });

router.use(function(req, res, next) {
    console.log("API is being called");
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

router.get('/', function(req, res) {
    res.json({
        message: 'Welcome to the API'
    });
});

router.get('/staff', function(req, res){
     
    //     {name: "Kieran McCarthy"},
    //     {name: "Alan Crowly"},
    //     {name: "Stephen Breen"}, 
    //     {name: "Elon Musk"}, 
    //     {name: "Alan Healy"}, 
    //     {name: "Richie Draper"}];
    
    // Array to hold the objects
    var staff = [];

    console.log('All Staff.');
    // Read all rows from table
    // request = new Request(
    //     "SELECT * FROM [dbo].[User]",
    //     function(err, rowCount, rows) {
    //         console.log(rowCount + ' row(s) returned');
    //         res.json({staff: staff});
    //     }
    // );
    // request.on('row', function(columns) {
    //     var obj = {};
    //     columns.forEach(function(column) {
    //         // Get the name of the Column and value and put in obj
    //         obj[column.metadata.colName] = column.value;
    //         console.log(column.metadata.colName, column.value);
    //     });
        
    //     // Push object into array
    //     staff.push(obj);
    // });
    // connection.execSql(request);
    var staff = [{Name: "Kieran McCarthy", in:true},
        {Name: "Alan Crowley", in:true},
        {Name: "Stephen Breen", in:true}, 
        {Name: "Elon Musk", in:false}, 
        {Name: "Alan Healy", in:true}, 
        {Name: "Richie Draper", in:false}];
// var obj =  {Name: "Richie Draper", in:true}
        // staff.push(obj);
        res.json({staff: staff});
});

router.get('/tables', function GetAllTables(req, res){
        
    var tables = [];
    console.log('Getting all tables.');
    // // Read all rows from table
    // request = new Request(
    //     "SELECT * FROM [dbo].[officeTable]",
    //     function(err, rowCount, rows) {
    //         console.log(rowCount + ' row(s) returned');
    //         res.json({tables: tables});
    //     }
    // );
    // request.on('row', function(columns) {
    //     var obj = {};
    //     columns.forEach(function(column) {
    //         obj[column.metadata.colName] = column.value;
    //         console.log(column.metadata.colName, column.value);
    //     });
        var tables = [{name:"table1", tableid:1, booked:true, bookedby:"TestUser"},
        {name:"table2", tableid:2, booked:true, bookedby:"Alan Crowley"},
        {name:"table3", tableid:3, booked:true, bookedby:"Richie Draper"},
        {name:"table4", tableid:4, booked:false, bookedby:null},
        {name:"table5", tableid:5, booked:false, bookedby:null}
        ];

         res.json({tables: tables});
    // connection.execSql(request);
});

router.post('/tables/:id/name/:name', function GetTablesByName(req, res){
    
    var id = req.params.id;
    var name = req.params.name;
    var table;
    console.log('Reading rows from the OfficeTable Table By name.');

    // Read all rows from table
    request = new Request(
        "update [officeTable] set [BOOKEDBY] = '" + name + "' where [ID] = " + id + ";",
        function(err, rowCount, rows) {
            
            console.log(rowCount + ' row(s) returned');
            
            res.json({success: "Updated!"});
        }
    );

    connection.execSql(request);
});

router.get('/tables/:id', function GetTableById(req, res){
        
    var id = req.params.id;
    var table;
    console.log('Reading rows from the OfficeTable Table By ID.');

    // Read all rows from table
    request = new Request(
        "SELECT * FROM [dbo].[officeTable] WHERE [ID] = " + id,
        function(err, rowCount, rows) {
            console.log(rowCount + ' row(s) returned');
            res.json({table: table});
        }
    );

    request.on('row', function(columns) {
        var obj = {};
        columns.forEach(function(column) {
            obj[column.metadata.colName] = column.value;
            console.log(column.metadata.colName, column.value);
        });
        table = obj;
    });
    connection.execSql(request);
});

router.get('/clients', function GetAllClients(req, res){
    var clients = [];
    console.log('Reading rows from the Client Table...');
    clients = [{

    }]
    // Read all rows from table
    request = new Request(
        "SELECT * FROM [dbo].[Client]",
        function(err, rowCount, rows) {
            console.log(rowCount + ' row(s) returned');
            res.json({clients: clients});
        }
    );

    request.on('row', function(columns) {
        var obj = {};
        columns.forEach(function(column) {
            obj[column.metadata.colName] = column.value;
            console.log(column.metadata.colName, column.value);
        });
        clients.push(obj);
    });
    connection.execSql(request);
});

module.exports = router;