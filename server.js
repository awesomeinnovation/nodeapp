var express = require("express");

var app = express();

var port = process.env.PORT || 8080;

var apiRoute = require("./routes/api.js");
var opsRoute = require("./routes/ops.js");

app.use(express.static(__dirname + "/public"));
app.set('view engine', 'ejs');

// Create connection to database

app.use('/api', apiRoute);
app.use('/ops', opsRoute);

app.get('/', function(req, res){
    res.render('index');
});

app.listen(port, function(){
    console.log("Listening on port 8080");
});

