var IOTApp = angular.module('IOTApp', ['chart.js']);

IOTApp.controller('HelloController', ['$scope', 'azureData', function($scope, azureData) {
    
        azureData.getStaff().success(function(data){
            console.log(data);
            $scope.office = data.staff;
        });

        azureData.getTables().success(function(data){
          console.log(data);
          $scope.tables = data.tables;
        });
}]);

IOTApp.controller('GraphsController', ['$scope', 'azureData', '$interval', function($scope, azureData, $interval) {
  
    // BARCHART
    $scope.labels = ['2016', '2017'];
      $scope.series = ['Series A', 'Series B'];
    
      $scope.data = [
        [65, 59, 80, 81, 56, 55, 40],
        [28, 48, 40, 19, 86, 27, 90],
        [33, 12, 45, 88, 56, 44, 40],
        [28, 48, 40, 19, 20, 59, 38]
      ];
      
    // CURVE GRAPH
    $scope.graph_labels = ["January", "February", "March", "April", "May", "June", "July"];
      $scope.graph_series = ['Series A', 'Series B'];
      $scope.graph_data = [
        [65, 59, 80, 81, 56, 55, 40],
        [28, 48, 40, 19, 86, 27, 90]
      ];
      $scope.onClick = function (points, evt) {
        console.log(points, evt);
      };
      $scope.graph_datasetOverride = [{ yAxisID: 'y-axis-1' }, { yAxisID: 'y-axis-2' }];
      $scope.graph_options = {
        scales: {
          yAxes: [
            {
              id: 'y-axis-1',
              type: 'linear',
              display: true,
              position: 'left'
            },
            {
              id: 'y-axis-2',
              type: 'linear',
              display: true,
              position: 'right'
            }
          ]
        }
      };
      
    //HOR BAR
    azureData.getClients().success(function(data){
      var clientNameArray = [];
      var clientTicketArray = [];
      data.clients.forEach(function(data){
        clientNameArray.push(data.NAME);
        clientTicketArray.push(data.TICKETS);
      });
      
      $scope.hor_labels = clientNameArray;
      $scope.hor_data = clientTicketArray;
    });
    
    $scope.hor_series = ['Series A'];
      
    // LINEBAR
    $scope.linebar_colors = ['#45b7cd', '#ff6384', '#ff8e72'];
    
    azureData.getStaff().success(function(data){
      var staffNameArray = [];
      var staffTicketArray = [];
      var staffOldArray = [28, 48, 40, 55, 86, 21, 30];
      var staffScoreArray = [];
      data.staff.forEach(function(data){
        staffNameArray.push(data.Name);
        staffTicketArray.push(data.TICKETS);
      });
      
      staffScoreArray.push(staffTicketArray);
      staffScoreArray.push(staffOldArray);
      
      console.log(staffScoreArray);
      
      $scope.linebar_labels = staffNameArray;
      $scope.linebar_data = staffScoreArray;
    })

    $scope.datasetOverride = [
      {
        label: "Bar chart",
        borderWidth: 1,
        type: 'bar'
      },
      {
        label: "Line chart",
        borderWidth: 3,
        hoverBackgroundColor: "rgba(255,99,132,0.4)",
        hoverBorderColor: "rgba(255,99,132,1)",
        type: 'line'
      }
    ];
    
}]);

IOTApp.factory('azureData', ['$http', function($http) {
    return {
        getStaff: function(){
            return $http.get('/api/staff');
        },
        
        getTables: function(){
            return $http.get('/api/tables');
        },
        
    };
}]);

// function callAPI(){
//     azureData.getStaff().success(function(data){
//         console.log(data);
//         $scope.office = data.staff;
//     });
    
//     console.log("HI");
// }
